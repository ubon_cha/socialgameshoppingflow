//衣装（スキン）のクラス
public class Costume extends Product {

	public Costume(int productNum, String name, String description, int price) {
		super(productNum, name, description, price);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	static Costume cos1 = new Costume(100, "常設衣装", "いつでも置いてます", 700);
	static Costume cos2 = new Costume(101, "季節衣装", "1/1~3/31限定", 1000);
	static Costume cos3 = new Costume(102, "イベント衣装", "イベント期間限定", 1200);

	/**コスチュームカテゴリの商品一覧を表示するメソッド
	 *
	 */
	public static void showCostumeAssortment() {
		// TODO 自動生成されたメソッド・スタブ

		System.out.println("1."+cos1.getName()+" "+cos1.getDescription()+" "+cos1.getPrice()+"課金通貨");
		System.out.println("2."+cos2.getName()+" "+cos2.getDescription()+" "+cos2.getPrice()+"課金通貨");
		System.out.println("3."+cos3.getName()+" "+cos3.getDescription()+" "+cos3.getPrice()+"課金通貨");
		System.out.println("購入する商品を選択してください");
		selectCostumeAssortment();
	}

	/**商品一覧から商品を選択させるメソッド
	 *
	 */
	public static void selectCostumeAssortment(){
		int selectNum = new java.util.Scanner(System.in).nextInt();
		if(selectNum == 1){
			confirmPurchase(cos1);
		}else if(selectNum == 2){
			confirmPurchase(cos2);
		}else if(selectNum == 3){
			confirmPurchase(cos3);
		}
	}

	/**選択された商品を購入するか確認するメソッド
	 * @param cos
	 */
	public static void confirmPurchase(Costume cos){
		System.out.println(cos.getName()+"を購入しますか？");
		System.out.println("1.購入する, 2.キャンセル");
		int num = new java.util.Scanner(System.in).nextInt();
		if(num == 1){
			System.out.println("購入ありがとうございました");
			ReceptionCounter.welcome();
		}else if(num == 2){
			ReceptionCounter.welcome();
		}
	}
}
