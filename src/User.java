//ユーザー情報を管理するクラス
public class User {

	String id;
	String password;
	String mailAddress;
	int billingAmount;

	/**コンストラクタ
	 * @param id
	 * @param password
	 * @param mailAddress
	 */
	public User(String id, String password, String mailAddress){
		this(id, password);
		this.mailAddress = mailAddress;
	}
	/**コンストラクタ
	 * @param id
	 * @param password
	 */
	public User(String id, String password){
		this.id = id;
		this.password = password;
	}

	/**課金通貨を増やすメソッド
	 * @param difference
	 */
	public void addBillingAmount(int difference){
		this.billingAmount += difference;
	}

	/**課金通貨を減らすメソッド
	 * @param difference
	 */
	public void reduceBillingAmount(int difference){
		this.billingAmount -= difference;
	}

	/**IDを引数にしてパスワードを渡すメソッド
	 * @param id
	 * @return
	 */
	public String bringPassword(String id){
		return this.password;
	}




	//ゲッターとセッター
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMailAddress() {
		return mailAddress;
	}

	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}

	public int getBillingAmount() {
		return billingAmount;
	}

	public void setBillingAmount(int billingAmount) {
		this.billingAmount = billingAmount;
	}





}
