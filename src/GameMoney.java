//商品としての課金通貨のクラス
public class GameMoney extends Product {

	public GameMoney(int productNum, String name, String description, int price) {
		super(productNum, name, description, price);
		// TODO 自動生成されたコンストラクター・スタブ
	}

	static Product gm1000 = new GameMoney(0, "課金通貨1000枚", "通常価格", 2000);
	static Product gm2000 = new GameMoney(0, "課金通貨2000枚", "割引価格", 3600);
	static Product gm3000 = new GameMoney(0, "課金通貨3000枚", "初回限定", 5100);

	/**課金通貨カテゴリの商品一覧を表示するメソッド
	 *
	 */
	public static void showGameMoneyAssortment() {
		// TODO 自動生成されたメソッド・スタブ
		System.out.println("1."+gm1000.getName()+" "+gm1000.getDescription()+" "+gm1000.getPrice()+"円");
		System.out.println("2."+gm2000.getName()+" "+gm2000.getDescription()+" "+gm2000.getPrice()+"円");
		System.out.println("3."+gm3000.getName()+" "+gm3000.getDescription()+" "+gm3000.getPrice()+"円");
		System.out.println("購入する商品を選択してください");
		selectGameMoneyAssortment();
	}

	/**商品一覧から商品を選択させるメソッド
	 *
	 */
	public static void selectGameMoneyAssortment(){
		int selectNum = new java.util.Scanner(System.in).nextInt();
		if(selectNum == 1){
			confirmPurchase(gm1000);
		}else if(selectNum == 2){
			confirmPurchase(gm2000);
		}else if(selectNum == 3){
			confirmPurchase(gm3000);
		}
	}

	/**選択された商品を購入するか確認するメソッド
	 * @param gm
	 */
	public static void confirmPurchase(Product gm){

		System.out.println(gm.getName()+"を購入しますか？");
		System.out.println("1.購入する, 2.キャンセル");
		int num = new java.util.Scanner(System.in).nextInt();
		if(num == 1){
			System.out.println("購入ありがとうございました");
			//ここで購入したユーザーの課金通貨を増やす処理をしたい。
			ReceptionCounter.welcome();
		}else if(num == 2){
			ReceptionCounter.welcome();
		}
	}
}
