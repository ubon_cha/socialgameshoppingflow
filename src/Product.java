//全ての商品クラスが継承する抽象クラス
public abstract class Product {

	private int num;
 	private String name;
	private String description;
	private int price;

	/**コンストラクタ
	 * @param productNum
	 * @param name
	 * @param description
	 * @param price
	 */
	public Product(int productNum, String name, String description, int price) {
		super();
		this.num = productNum;
		this.name = name;
		this.description = description;
		this.price = price;
	}

	//ゲッターとセッター
	public int getProductNum() {
		return num;
	}
	public void setProductNum(int productNum) {
		this.num = productNum;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
}
